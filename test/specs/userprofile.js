var assert = require('chai').assert;
var expect = require('chai').expect;

describe ('user sign up', function ()
         {
           this.timeout(50000);
           browser.timeouts("implicit",10000);
           it("login",function ()
            {
              browser.url('http://localhost:3000');
              browser.click('.user');
              browser.setValue('#identifier','user1@gmail.com');
              browser.setValue('#password','techno');
              browser.click ('#login-submit');
             });

           it('personal Details Check', function ()
           	{
           	  browser.click ('#tab5');
              browser.click('#personallink');	
              browser.click ('#editpersonaldetails');
              browser.setValue('#firstname','Siva');
              browser.setValue('#lastname','Kumar');
              browser.setValue('#fathername','apparao');
              browser.click('#male');
              browser.click('#female');
              browser.click('#single');
              browser.click('#married');
              browser.setValue('#nationality','Indian');
              browser.click('#residentindividual');
              browser.click('#personalbtn');
              browser.pause (1000);
              var f = browser.getText ('#valperfirstname');
              assert.equal (f , 'Siva');
              var l = browser.getText ('#valperlastname');
              assert.equal (l , 'Kumar');
              var d = browser.getText ('#valperdob');
              assert.equal (d , '2016-Dec-27');
              var fn = browser.getText ('#valperfathername');
              assert.equal (fn , 'apparao');
              var g = browser.getText ('#valpergender');
              assert.equal (g , 'Female');
              var ms = browser.getText ('#valpermaritalstatus');
              assert.equal (ms , 'Married');
              var na = browser.getText ('#valpernationality');
              assert.equal (na , 'Indian');
              var s = browser.getText ('#valperstatus');
              assert.equal (s , 'Resident Individual');
             });

           it('contact Details Check',function ()
            {
              browser.click('#addresslink');	
              browser.click ('#editContactInfo');
              browser.element('#presentaddress').setValue('#housenumber', '1-56/41');
              browser.element('#presentaddress').setValue('#landmark', 'JNTU Road');
              browser.element('#presentaddress').setValue('#streetname', 'kphp');
              browser.element('#presentaddress').setValue('#villagename', 'housing board');
              browser.element('#presentaddress').selectByValue('#stateid', '2');
              browser.pause (1000);
              browser.element('#presentaddress').selectByValue('#districtid', '7');
              browser.element('#presentaddress').setValue('#pincode', '530041');
              browser.element('#presentaddress').setValue('#alternativecontactnumber',
               '9985427959');
              
              browser.element('#permanentaddress').setValue('#housenumber', '1-56/3');
              browser.element('#permanentaddress').setValue('#landmark', 'YSR Circket Stadium');
              browser.element('#permanentaddress').setValue('#streetname', 'sample');
              browser.element('#permanentaddress').setValue('#villagename', 'sample.villagename');
              browser.element('#permanentaddress').selectByValue('#stateid', '1');
              browser.pause (1000);
              browser.element('#permanentaddress').selectByValue('#districtid', '2');
              browser.element('#permanentaddress').setValue('#pincode', '530042');
              browser.element('#permanentaddress').setValue('#alternativecontactnumber',
               '8500487583');

              browser.setValue('#mobilenumber', '8500487583');
              browser.setValue('#landlinenumber', '9985427959');
              browser.setValue('#email', 'siva@gmail.com');
	          browser.setValue('#telres', '666-666-666');
              browser.setValue('#fax', '999-999-999');  
              browser.click('#addressbtn');
              browser.pause (1000);

              var hnum = browser.getText ('#valprehousenumber');
              assert.equal (hnum , '1-56/41');
              var lmark = browser.getText ('#valprelandmark');
              assert.equal (lmark , 'JNTU Road');
              var street = browser.getText ('#valprestreetname');
              assert.equal (street , 'kphp');
              var village = browser.getText ('#valprevillagename');
              assert.equal (village , 'housing board');
              var state = browser.getText ('#valprestatename');
              assert.equal (state, 'Telangana');
              var district = browser.getText ('#valpredistrictname');
              assert.equal (district , 'Hyderabad');
              var pin = browser.getText ('#valprepincode');
              assert.equal (pin , '530041');
              var altnumber = browser.getText ('#valprealtnumber');
              assert.equal (altnumber , '9985427959'); 

              var hnum1 = browser.getText ('#valperhousenumber');
              assert.equal (hnum1 , '1-56/3');
              var lmark1 = browser.getText ('#valperlandmark');
              assert.equal (lmark1 , 'YSR Circket Stadium');
              var street1 = browser.getText ('#valperstreetname');
              assert.equal (street1 , 'sample');
              var village1 = browser.getText ('#valpervillagename');
              assert.equal (village1 , 'sample.villagename');
              var state1 = browser.getText ('#valperstatename');
              assert.equal (state1, 'Andhra Pradesh');
              var district1 = browser.getText ('#valperdistrictname');
              assert.equal (district1 , 'Guntur');
              var pin1 = browser.getText ('#valperpincode');
              assert.equal (pin1 , '530042');
              var altnumber1 = browser.getText ('#valperaltnumber');
              assert.equal (altnumber1 , '8500487583');    

              var mobile = browser.getText ('#valcmobilenumber');
              assert.equal (mobile , '8500487583');
              var landline = browser.getText ('#valclandline');
              assert.equal (landline , '9985427959');
              var email = browser.getText ('#valcemail');
              assert.equal (email , 'siva@gmail.com');
              var tel = browser.getText ('#valctelres');
              assert.equal (tel , '666-666-666');
              var fax = browser.getText ('#valcfax');
              assert.equal (fax , '999-999-999');        
            });

          it('Contact Details Copy Functionality',function ()
            {
              browser.click('#addresslink');	
              browser.click ('#editContactInfo');
              browser.element('#presentaddress').setValue('#housenumber', '1-56/41');
              browser.element('#presentaddress').setValue('#landmark', 'JNTU Road');
              browser.element('#presentaddress').setValue('#streetname', 'kphp');
              browser.element('#presentaddress').setValue('#villagename', 'housing board');
              browser.element('#presentaddress').selectByValue('#stateid', '2');
              browser.pause (1000);
              browser.element('#presentaddress').selectByValue('#districtid', '7');
              browser.element('#presentaddress').setValue('#pincode', '530041');
              browser.element('#presentaddress').setValue('#alternativecontactnumber',
               '9985427959');
              browser.click('#copy');
              browser.setValue('#mobilenumber', '8500487583');
              browser.setValue('#landlinenumber', '9985427959');
              browser.setValue('#email', 'siva@gmail.com');
	          browser.setValue('#telres', '666-666-666');
              browser.setValue('#fax', '999-999-999');  
              browser.click('#addressbtn');
              browser.pause (1000);

              var hnum2 = browser.getText ('#valprehousenumber');
              assert.equal (hnum2 , '1-56/41');
              var lmark2 = browser.getText ('#valprelandmark');
              assert.equal (lmark2 , 'JNTU Road');
              var street2 = browser.getText ('#valprestreetname');
              assert.equal (street2 , 'kphp');
              var village2 = browser.getText ('#valprevillagename');
              assert.equal (village2 , 'housing board');
              var state2 = browser.getText ('#valprestatename');
              assert.equal (state2, 'Telangana');
              var district2 = browser.getText ('#valpredistrictname');
              assert.equal (district2 , 'Hyderabad');
              var pin2 = browser.getText ('#valprepincode');
              assert.equal (pin2 , '530041');
              var altnumber2 = browser.getText ('#valprealtnumber');
              assert.equal (altnumber2 , '9985427959'); 

              var hnum3 = browser.getText ('#valperhousenumber');
              assert.equal (hnum3 , '1-56/41');
              var lmark3 = browser.getText ('#valperlandmark');
              assert.equal (lmark3 , 'JNTU Road');
              var street3 = browser.getText ('#valperstreetname');
              assert.equal (street3 , 'kphp');
              var village3 = browser.getText ('#valpervillagename');
              assert.equal (village3 , 'housing board');
              var state3 = browser.getText ('#valperstatename');
              assert.equal (state3, 'Telangana');
              var district3 = browser.getText ('#valperdistrictname');
              assert.equal (district3 , 'Hyderabad');
              var pin3 = browser.getText ('#valperpincode');
              assert.equal (pin3 , '530041');
              var altnumber3 = browser.getText ('#valperaltnumber');
              assert.equal (altnumber3 , '9985427959');    

              var mobile = browser.getText ('#valcmobilenumber');
              assert.equal (mobile , '8500487583');
              var landline = browser.getText ('#valclandline');
              assert.equal (landline , '9985427959');
              var email = browser.getText ('#valcemail');
              assert.equal (email , 'siva@gmail.com');
              var tel = browser.getText ('#valctelres');
              assert.equal (tel , '666-666-666');
              var fax = browser.getText ('#valcfax');
              assert.equal (fax , '999-999-999');        
            });

          it('Professional Details Check and validate',function ()
            {
              browser.click('#primarylink');	
              browser.click ('#editProfessional');
              browser.setValue('#employmentType', 'Permanent');
              browser.setValue('#companyName', 'Technoidentity');
              browser.setValue('#designation', 'Clark');
              browser.element('#employementdetails').setValue('#description', 'happy');
              browser.setValue('#numberOfJobChanges', '5');
              browser.setValue('#year','2');
              browser.setValue('#month','6');
              browser.element('#officeaddress').setValue('#housenumber', '1-56/41');
              browser.element('#officeaddress').setValue('#landmark', 'JNTU Road');
              browser.element('#officeaddress').setValue('#streetname', 'kphp');
              browser.element('#officeaddress').setValue('#villagename', 'housing board');
              browser.element('#officeaddress').selectByValue('#stateid', '2');
              browser.pause (1000);
              browser.element('#officeaddress').selectByValue('#districtid', '7');
              browser.element('#officeaddress').setValue('#pincode', '530041');
              browser.element('#officeaddress').setValue('#alternativecontactnumber',
               '9985427959');

              browser.setValue('#school', 'AU');
              browser.setValue('#endYear', '2015');
              browser.setValue('#degree', 'M.Sc Computers & Statistics');
              browser.setValue('#fieldOfStudy', 'Computers & Statistics');
              browser.element ('#educationaldetails').setValue('#description', 'education happy');
              

              browser.setValue('#name', 'reference');
              browser.setValue('#email', 'reference@gmail.com');
              browser.setValue('#phoneNumber', '9985427959');
              browser.element ('#referencedetails').setValue('#description', 'reference happy');           
              
              browser.click ('#professionalbtn');
              browser.pause (1000);

              var emptype = browser.getText ('#valemptype');
              assert.equal (emptype , 'Permanent');
              var compname = browser.getText ('#valcompname');
              assert.equal (compname , 'Technoidentity');
              var empdesig = browser.getText ('#valempdesig');
              assert.equal (empdesig , 'Clark');
              var empdes = browser.getText ('#valempdes');
              assert.equal (empdes , 'happy');
              var numch = browser.getText ('#valnumchg');
              assert.equal (numch , '5');
              var wrkexp = browser.getText ('#valempexp');
              assert.equal (wrkexp , '2 Years 6 Months');


     		  var hnum4 = browser.getText ('#valoffhousenumber');
              assert.equal (hnum4 , '1-56/41');
              var lmark4 = browser.getText ('#valofflandmark');
              assert.equal (lmark4 , 'JNTU Road');
              var street4 = browser.getText ('#valoffstreetname');
              assert.equal (street4 , 'kphp');
              var village4 = browser.getText ('#valoffvillagename');
              assert.equal (village4 , 'housing board');
              var state4 = browser.getText ('#valoffstatename');
              assert.equal (state4, 'Telangana');
              var district4 = browser.getText ('#valoffdistrictname');
              assert.equal (district4 , 'Hyderabad');
              var pin4 = browser.getText ('#valoffpincode');
              assert.equal (pin4 , '530041');
              var altnumber4 = browser.getText ('#valoffaltnumber');
              assert.equal (altnumber4 , '9985427959'); 

              var school = browser.getText ('#valschool');
              assert.equal (school , 'AU');
              var gradyear = browser.getText ('#valgrayear');
              assert.equal (gradyear , '2015');
              var degree = browser.getText ('#valdegree');
              assert.equal (degree , 'M.Sc Computers & Statistics');
              var qual = browser.getText ('#valqualification');
              assert.equal (qual , 'Computers & Statistics');
              var edudes = browser.getText ('#valedudescription');
              assert.equal (edudes , 'education happy');  

              var ref = browser.getText ('#valrefname');
              assert.equal (ref , 'reference');
              var refemail = browser.getText ('#valrefemailid');
              assert.equal (refemail , 'reference@gmail.com');
              var refphone = browser.getText ('#valrefphonenumber');
              assert.equal (refphone , '9985427959');
              var refdes = browser.getText ('#valrefdescription');
              assert.equal (refdes , 'reference happy');  
            });


 });

