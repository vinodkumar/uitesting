var assert = require('chai').assert;
var expect = require('chai').expect;

describe('user sign up',function ()
         {
             this.timeout(50000);
             browser.timeouts("implicit",10000);
             it('signup required validations',function ()
                {
                    browser.url('http://localhost:3000');
                    browser.click('.user');
                    browser.click('#user-signup');
                    browser.pause(2000);
                    browser.click('#login-submit');
                    var values = browser.getText('p*=required');
                    expect(values).to.eql(['First name is required',
                                       'Last name is required',
                                       'Mobile number is required',
                                       'Email id is required',
                                       'Password is required',
                                       'Confirm password is required',
                                       'Date of Birth is required']);
                })

              it('signup user input data validations',function ()
                {
                    browser.url('http://localhost:3000');
                    browser.click('.user');
                    browser.click('#user-signup');
                    browser.setValue('#firstName','asdfsd');
                    browser.setValue('#lastName','asdfsd');
                    browser.setValue('#mobileNumber','fgdffsd');
                    browser.setValue('#email',"a");
                    browser.setValue('//*[@id="password.main"]',"asdfsdgdfg");
                    browser.setValue('//*[@id="password.confirm"]',"dgdfgd");
                    browser.click('#login-submit');
                    var values = browser.getText('p*=valid');
                    values.push(browser.getText('p*=Password & conform password must be same'));
                    expect(values).to.eql(['Enter valid mobile number',
                                       'Enter valid email id',
                                       'Password & conform password must be same']);
                })

               it('already existing user validation',function ()
                {
                    browser.url('http://localhost:3000');
                    browser.click('.user');
                    browser.click('#user-signup');
                    browser.setValue('#firstName','asdfsd');
                    browser.setValue('#lastName','asdfsd');
                    browser.setValue('#mobileNumber',9000000000);
                    browser.setValue('#email',"inturi99@gmail.com");
                    browser.setValue('//*[@id="password.main"]',"dgdfgd");
                    browser.setValue('//*[@id="password.confirm"]',"dgdfgd");
                    browser.click('#male');
                    browser.click("//*[@id='main-app-area']/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[8]/div/div[1]/div[1]/span");
                    browser.click("//*[@id='main-app-area']/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[8]/div/div[1]/div[2]/table/tbody/tr[2]/td[5]");
                    browser.click('//*[@id="login-submit"]');
                    browser.pause(2000);
                    var etext = browser.getText('#server-status');
                    assert.equal(etext, "User already exists","test case failed");
                })

               it('new user creation',function ()
                {
                    browser.url('http://localhost:3000');
                    browser.click('.user');
                    browser.click('#user-signup');
                    browser.setValue('#firstName','rajesh');
                    browser.setValue('#lastName','P');
                    browser.setValue('#mobileNumber',9000000000);
                    browser.setValue('#email',"rajeshreddyponnala@gmail.com");
                    browser.setValue('//*[@id="password.main"]',"Design_20");
                    browser.setValue('//*[@id="password.confirm"]',"Design_20");
                    browser.click('#male');
                    browser.click("//*[@id='main-app-area']/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[8]/div/div[1]/div[1]/span");
                    browser.click("//*[@id='main-app-area']/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[8]/div/div[1]/div[2]/table/tbody/tr[2]/td[5]");
                    browser.click('//*[@id="login-submit"]');
                    browser.pause(2000);
                    var etext = browser.getText('#server-status');
                    assert.equal(etext,"Please check your mail","test case failed");
                })

             ;});
