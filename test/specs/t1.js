var assert = require('chai').assert;

describe('my profile',function (){
  this.timeout(50000);
  browser.timeouts('implicit', 15000);
  browser.timeouts('page load',15000);

   it('login validations/incorrect fields',function ()
      {
     browser.url('http://localhost:3000/');
     browser.click('.user');
     browser.setValue('#identifier','inturi99@gmail.com');
     browser.setValue('#password','Design');
     browser.click('#login-submit');
     browser.pause(2000);
     var value=browser.getText('.//*[@id="main-app-area"]/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[3]/b');
     assert.equal(value,'invalid credentials','logging in with invalid credentials');

   });

    it('login validations/both fields are empty',function ()
       {
      browser.url('http://localhost:3000/');
      browser.click('.user');
      browser.setValue('#identifier','');
      browser.setValue('#password','');
      browser.click('#login-submit');
      var value1=browser.getText('.//*[@id="main-app-area"]/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[1]/div/div/div/b');
      var value2=browser.getText('.//*[@id="main-app-area"]/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div/b');
      assert.equal(value1,'Field is required','validations not working for Email-id');
      assert.equal(value2,'Field is required','validations not working for password');
    });



   it('login validations/username field is empty and valid password',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    var value1=browser.getText('.//*[@id="main-app-area"]/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[1]/div/div/div/b');
     assert.equal(value1,'Field is required','validations not working for Email-id');
    });

  it('login validations/password field is empty and valid email-id',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','');
    browser.click('#login-submit');
    var value2=browser.getText('.//*[@id="main-app-area"]/div/div/div/div/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div/b');
    assert.equal(value2,'Field is required','validations not working for password');
  });

  it('login validations/both fields are correct',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    var value=browser.isVisible('#tab1');
    assert.equal(value,true,'not logged in')
 });

   it('Get firstname',function ()
     {
     browser.click('#tab2');
     browser.setValue('#firstname','krishnarao');
     browser.click('#personalbtn');
     var firstname=browser.getValue('#firstname');
     assert.equal(firstname,'krishnarao','firstname not matching');
  });


  it('Get lastname',function ()
     {
    browser.click('#personallink');
    browser.setValue('#lastname','inturi');
    browser.click('#personalbtn');
    var lastname =browser.getValue('#lastname');
    lastname.should.equal('inturi');
  });

  it('Get fathername',function ()
     {
    browser.click('#personallink');
    browser.setValue('#fathername','Brahmaiah');
    browser.click('#personalbtn');
    var fathername =browser.getValue('#fathername');
    fathername.should.equal('Brahmaiah');
  });

  it('Get gender/male',function ()
     {
    browser.click('#personallink');
    browser.click('#male');
    browser.click('#personalbtn');
    var male = browser.getAttribute('#male','checked');
    assert.equal(male,'true','gender male is not checked');
  });

    it('Get nationality',function ()
     {
    browser.click('#personallink');
    browser.setValue('#nationality','India');
    browser.click('#personalbtn');
    var nationality =browser.getValue('#nationality');
    nationality.should.equal('India');
  });


 it('resident individual',function ()
    {
    browser.click('#personallink');
   browser.click('#residentindividual');
   browser.click('#personalbtn');
   var resident = browser.getAttribute('#residentindividual','checked');
   assert.equal(resident,null,'resident individual is not checked');
 });

  it('get housenumber',function ()
     {
    browser.setValue ('#housenumber','5-4-100');
    browser.click('#addressbtn');
    var housenumber = browser.getValue('#housenumber')
    assert.equal(housenumber,'5-4-100')
  });

  it('get landmark',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#landmark','JNTU');
    browser.click('#addressbtn');
    var landmark =browser.getValue('#landmark')
    assert.equal(landmark,'JNTU')
  });

  it('get streetname',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#streetname','KPHB');
    browser.click('#addressbtn');
    var streetname =browser.getValue('#streetname')
    assert.equal(streetname,'KPHB')
  });

  it('get village name',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#villagename','Kukatpally');
    browser.click('#addressbtn');
    var villagename =browser.getValue('#villagename')
    assert.equal(villagename,'Kukatpally')
  });

  it('get stateid',function ()
     {
    browser.click('#addresslink');
    browser.selectByValue('#stateid',1);
    browser.click('#addressbtn');
    var stateid =browser.getValue('#stateid')
    assert.equal(stateid,1)
  });

  it('get Districtid',function ()
     {
    browser.click('#addresslink');
    browser.selectByValue('#districtid',1);
    browser.click('#addressbtn');
    var districtid =browser.getValue('#districtid')
    assert.equal(districtid,1)
  });

  it('get pincode',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#pincode','500085');
    browser.click('#addressbtn');
    var pincode =browser.getValue('#pincode')
    assert.equal(pincode,'500085')
  });

  it('get mobile number',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#mobilenumber','9966126757');
    browser.click('#addressbtn');
    var mobilenumber =browser.getValue('#mobilenumber')
    assert.equal(mobilenumber,'9966126757')
  });

  it('get landline',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#landlinenumber','66646546546');
    browser.click('#addressbtn');
    var landline =browser.getValue('#landlinenumber')
    assert.equal(landline,'66646546546')
  });

  it('get email',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#email','inturi99@gmail.com');
    browser.click('#addressbtn');
    var email =browser.getValue('#email')
    assert.equal(email,'inturi99@gmail.com')
  });

  it('get telres',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#telres','34324324');
    browser.click('#addressbtn');
    var telres =browser.getValue('#telres')
    assert.equal(telres,'34324324')
  });

  it('get fax',function ()
     {
    browser.click('#addresslink');
    browser.setValue('#fax','34324');
    browser.click('#addressbtn');
    var fax =browser.getValue('#fax')
    assert.equal(fax,'34324')
  });

  it('get loan speciality',function ()
     {
    browser.click('#loanslink');
    browser.click('#SMELoans');
    browser.click('#loanstype');
    var smeloans = browser.getAttribute('#SMELoans','checked')
    assert.equal(smeloans,'true','sme loans are not checked')
  });

  it('my profile-personal details tab ',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab2');
    browser.click('#personallink');
    var value=browser.isVisible('#firstname');
    assert.equal(value,true,'personal details tab is not working')
  });

  // it('personal details/validations when fields are empty',function ()
  //    {
  //     browser.url('http://localhost:3000/');
  //     browser.click('.user');
  //     browser.setValue('#identifier','inturi99@gmail.com');
  //     browser.setValue('#password','Design_20');
  //     browser.click('#login-submit');
  //     browser.pause(10000);
  //     browser.click('#tab2');
  //     browser.pause(4000);
  //   browser.setValue('#firstname','sdfsdf');
  //   browser.pause(2000);
  //   browser.setValue('#firstname','');
  //   browser.setValue('#firstname','');
  //    browser.pause(2000);
  //     browser.setValue('#lastname','');
  //     // browser.pause(2000);
  //     browser.setValue('#fathername','');
  //    // browser.pause(2000);
  //     browser.setValue('#nationality','');
  //    // browser.pause(2000);
  //     browser.click('#personalbtn');
  //     var value=browser.getText('.//*[@id="personaldetails"]/div/div[1]/label');
  //     console.log(value);
  // });

   it('All correct personaldetails',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab2');
     browser.setValue('#firstname','gvk');
    browser.setValue('#lastname','g');
    browser.setValue('#fathername','ravi');
    browser.click('#male');
    browser.click('#single');
    browser.setValue('#nationality',"indian");
    browser.click('#residentindividual');
    browser.click('#personalbtn');
     var value= browser.isVisible('#housenumber');
    assert.equal(value,true,'not moved into the address details tab')
   });

 it('all correct address details',function ()
     {
   browser.url('http://localhost:3000/');
   browser.click('.user');
   browser.setValue('#identifier','inturi99@gmail.com');
   browser.setValue('#password','Design_20');
   browser.click('#login-submit');
   browser.pause(10000);
   browser.click('#tab2');
   browser.pause(2000);
   browser.click('#addresslink');
   browser.setValue('#housenumber','1-2-3');
   browser.setValue('#landmark','kukatpally');
   browser.setValue('#streetname','kphb');
   browser.setValue('#villagename','kukatpally');
   browser.setValue('#pincode','500085');
   browser.setValue('#mobilenumber','9705286834');
   browser.setValue('#landlinenumber','04082099');
   browser.setValue('#email','gvk@gmail.com');
   browser.click('#addressbtn');
   var value = browser.isVisible('#landmark');
   assert.equal(value,false,'not moved into the primary details tab');
});


  it('verification details-Aadhaar/check whether invalid document is saved/not',function ()
   {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab2');
    browser.click('#primarylink');
    browser.setValue('#aadhaarNumber','abcdefgh');
    browser.chooseFile('#aadhaar','/home/vinod/Downloads/signature.asc');
    browser.click('#saveaadhaarNumber');
    browser.pause(3000);
    var val1=browser.alertText();
   // console.log(val1);
    assert.typeOf(val1,'string','invalid images are saved');
    browser.alertAccept();
  });

  it('passport/check whether invalid document is saved/not',function ()
      {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab2');
    browser.click('#primarylink');
    browser.setValue('#passportNumber','abcdefgh');
    browser.chooseFile('#passport','/home/vinod/Downloads/signature.asc');
    browser.click('#savepassportNumber');
    browser.pause(2000);
    var val1=browser.alertText();
    assert.typeOf(val1,'string','invalid images are saved');
    browser.alertAccept();
  });

it('pancard/check whether invalid document is saved/not',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.chooseFile('#pancardno','/home/vinod/Downloads/signature.asc');
  browser.click('#savepancardNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});


  it('bank statement/check whether invalid document is saved/not',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.chooseFile('#bankstatement','/home/vinod/Downloads/signature.asc');
  browser.click('#savebankName');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});


  it('bank statement/check whether invalid document is saved/not',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.chooseFile('#bankstatement','/home/vinod/Downloads/signature.asc');
  browser.click('#savebankName');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});

it('telephone bill/check whether invalid document is saved/not',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.chooseFile('#telephonebill','/home/vinod/Downloads/signature.asc');
  browser.click('#savetelePhoneNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});



it('driving license/check whether invalid document is saved/not',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.chooseFile('#drivinglicense','/home/vinod/Downloads/signature.asc');
  browser.click('#savedrivingLicenseNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});


it('verification details-all details all fields with invalid documents',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab2');
  browser.click('#primarylink');
  browser.setValue('#aadhaarNumber','abcdefgh');
  browser.chooseFile('#aadhaar','/home/vinod/Downloads/signature.asc');
  browser.click('#saveaadhaarNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
  browser.setValue('#passportNumber','abcdefgh');
  browser.chooseFile('#passport','/home/vinod/Downloads/signature.asc');
  browser.click('#savepassportNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();

  browser.setValue('#pancardNumber','abcdefgh');
  browser.chooseFile('#pancardno','/home/vinod/Downloads/signature.asc');
  browser.click('#savepancardNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
  browser.chooseFile('#bankstatement','/home/vinod/Downloads/signature.asc');
  browser.setValue('#bankName','abcdefgh');
  browser.click('#savebankName');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
  browser.setValue('#telePhoneNumber','abcdefgh');
  browser.chooseFile('#telephonebill','/home/vinod/Downloads/signature.asc');
  browser.click('#savetelePhoneNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
  browser.setValue('#drivingLicenseNumber','abcdefgh');
  browser.chooseFile('#drivinglicense','/home/vinod/Downloads/signature.asc');
  browser.click('#savedrivingLicenseNumber');
  browser.pause(2000);
  var val1=browser.alertText();
  assert.typeOf(val1,'string','invalid images are saved');
  browser.alertAccept();
});

  it('exam/question1 option1',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[1]/div/label/input');
    var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[1]/div/label/input','checked');
    assert.equal(value,'true',"option1 is not selected ");
   // console.log(browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[1]/div/label/input','checked'));
  });

it('exam/question1 option2',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab3');
  browser.selectByValue('#tutorialtype','Generalized');
  browser.click('#gotoexam');
  browser.click('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[2]/div/label/input');
  var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[2]/div/label/input','checked');
  assert.equal(value,'true','option2 is not selected');
  // console.log(browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[2]/div/label/input','checked'));
});

it('exam/question1 option3',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab3');
  browser.selectByValue('#tutorialtype','Generalized');
  browser.click('#gotoexam');
  browser.click('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[1]/div/label/input');
// console.log(browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[1]/div/label/input','checked'));
  var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[1]/div/label/input','checked');
  assert.equal(value,'true','option3 is not selected');
});

it('exam/question1 option4',function ()
   {
   browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab3');
  browser.selectByValue('#tutorialtype','Generalized');
  browser.click('#gotoexam');
  browser.click('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[2]/div/label/input');
   var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[2]/div/label/input','checked');
   assert.equal(value,'true','option 4 is not selected');
  //console.log(browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[2]/div[2]/div/label/input','checked'));
  });

it('exam/question2/ option1',function ()
   {
  browser.url('http://localhost:3000/');
  browser.click('.user');
  browser.setValue('#identifier','inturi99@gmail.com');
  browser.setValue('#password','Design_20');
  browser.click('#login-submit');
  browser.click('#tab3');
  browser.selectByValue('#tutorialtype','Generalized');
  browser.click('#gotoexam');
  browser.click('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[1]/div[1]/div/label/input');
  var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[1]/div[1]/div/label/input','checked');
  assert.equal(value,'true','option1 is not selected');
});

  it('exam/question2/ option2',function ()
   {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[1]/div[2]/div/label/input');
    var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[1]/div[2]/div/label/input','checked');
    assert.equal(value,'true','option2 is not selected');
  });

it('exam/question2/ option3',function ()
   {
  browser.click('#tab3');
  browser.selectByValue('#tutorialtype','Generalized');
  browser.click('#gotoexam');
  browser.click('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[2]/div[1]/div/label/input');
  var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[2]/div[1]/div/label/input','checked');
  assert.equal(value,'true','option3 is not selected');
});

  it('exam/question2/ option4',function ()
   {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[2]/div[2]/div/label/input');
    var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[2]/div[2]/div/label/input','checked');
    assert.equal(value,'true','option4 is not selected');
});

  it('exam/question3/ option1', function ()
  {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[1]/div/label/input');
   var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[1]/div/label/input','checked');
   assert.equal(value,'true','option 1 is not selected');
 });

 it('exam/question4/option2',function ()
   {
   browser.url('http://localhost:3000/');
   browser.click('.user');
   browser.setValue('#identifier','inturi99@gmail.com');
   browser.setValue('#password','Design_20');
   browser.click('#login-submit');
   browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
   browser.click('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[2]/div/label/input');
    var value = browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[2]/div/label/input','checked');
    assert.equal(value,'true','option 2 is not selected');
 });

  it('exam/question5/option3',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[4]/div/div/div[2]/div[1]/div/label/input');
    var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[4]/div/div/div[2]/div[1]/div/label/input','checked');
    assert.equal(value,'true','option 3 is not selected');
  });

  it('exam/question6/option1', function ()
  {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[5]/div/div/div[1]/div[1]/div/label/input');
   var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[5]/div/div/div[1]/div[1]/div/label/input','checked');
   assert.equal(value,'true','option 1 is not selected');
 });

 it('exam/question7/option4', function ()
  {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
   browser.click('.//*[@id="tab-holder"]/div/div/div[6]/div/div/div[2]/div[2]/div/label/input');
   var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[6]/div/div/div[2]/div[2]/div/label/input','checked');
   assert.equal(value,'true','option 4 is not selected');
 });

  it('exam/question8/option3', function ()
  {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[7]/div/div/div[2]/div[1]/div/label/input');
   var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[7]/div/div/div[2]/div[1]/div/label/input','checked');
   assert.equal(value,'true','option 3 is not selected');
 });

  it('exam/question9/option3', function ()
  {
     browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[9]/div/div/div[2]/div[1]/div/label/input');
    var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[9]/div/div/div[2]/div[1]/div/label/input','checked');
   assert.equal(value,'true','option 3 is not selected');
 });

 it('exam/question10/option4', function ()
  {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
   browser.click('.//*[@id="tab-holder"]/div/div/div[10]/div/div/div[2]/div[2]/div/label/input');
   var value =browser.getAttribute('.//*[@id="tab-holder"]/div/div/div[10]/div/div/div[2]/div[2]/div/label/input','checked');
   assert.equal(value,'true','option 4 is not selected');
 });

  it('exams/Answering all questions',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab3');
    browser.selectByValue('#tutorialtype','Generalized');
    browser.click('#gotoexam');
    browser.click('.//*[@id="tab-holder"]/div/div/div[1]/div/div/div[1]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[2]/div/div/div[2]/div[2]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[3]/div/div/div[1]/div[2]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[4]/div/div/div[2]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[5]/div/div/div[1]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[6]/div/div/div[2]/div[2]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[7]/div/div/div[2]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[9]/div/div/div[2]/div[1]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[10]/div/div/div[2]/div[2]/div/label/input');
    browser.click('.//*[@id="tab-holder"]/div/div/div[12]/div/button[1]');
    var value = browser.isVisible('.//*[@id="tab-holder"]/div/div/div[12]/div/button[2]');
    assert.equal(value,true,"submit button not working");
    browser.click('.//*[@id="tab-holder"]/div/div/div[12]/div/button[2]');
    browser.pause(2000);
    var value = browser.isVisible ('#myModalLabel');
    assert.equal(value,true,"results are not displayed");
  });

  it('myconnections get name',function ()
     {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab4');
    browser.click('#findinvite');
    browser.setValue('#setname','mahesh')
    var name=browser.getValue('#setname')
    assert.equal(name,'mahesh','name not matching')
  });

  it('myconnections get email',function ()
     {
     browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab4');
    browser.click('#findinvite')
    browser.setValue('#setemail','mahesh@gmail.com')
    var email=browser.getValue('#setemail')
    assert.equal(email,'mahesh@gmail.com')
  });

  it('my connections only name',function()
     {
     browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('#tab4');
    browser.click('#findinvite');
    browser.setValue('#setname','mahesh');
    browser.click('#saveconn')
    var fail=browser.getText('#savefailure')
    assert.equal(fail,'Oops! something went wrong try again','all details are not entered')
  });

 it('my connections all details filled',function ()
    {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
   browser.click('#tab4');
   browser.click('#findinvite');
   browser.setValue('#setname','ganesh');
   browser.setValue('#setemail','ganesh@gmail.com');
   browser.click('#individual');
   browser.click('#saveconn');
   var success=browser.getText('#savesuccess')
   assert.equal(success,'Successfull','invitations are not saved properly ')
 });

 it('FAQ',function()
 {
    browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
   browser.click('#tab5');
   var value = browser.isVisible('#faq');
   assert.equal(value,true,'questions are not displayed')
 });

  it('logout',function ()
     {
     browser.url('http://localhost:3000/');
    browser.click('.user');
    browser.setValue('#identifier','inturi99@gmail.com');
    browser.setValue('#password','Design_20');
    browser.click('#login-submit');
    browser.click('.//*[@id="myprofile"]/div/div/div[2]/ul/li[6]/a');
    var value =browser.isVisible('#home-form');
    assert.equal(value,true,'not logged out properly');
});

});
